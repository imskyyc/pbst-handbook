---
home: true
heroImage: /PBST-Logo.png
actionText: Ab geht's!
actionLink: pbst/
---

::: warning HINWEIS: Seit dem
 - `Europäisches Datumsformat: 9.3.2020`
 - `Amerikanisches Datumsformat: 3.9.2020`

Ist diese Version des Handbuchs ist ein offizielles PBST Handbuch. Wie Csdi zu diesem Datum erklärte: ![img](https://i.imgur.com/ZVyPdZb.png)

Das andere offizielle Handbuch findest du [hier](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368). Bitte unterstützt auch die offizielle Version :smile: 
:::

::: danger ACHTUNG! 
Bitte lies dir sorgfältig alle Regeln durch. Auf jegliche Änderungen wird hingewiesen. Die Regeln wurden in Kategorien eingeteilt, je nach dem auf welche Gruppe sie zutrifft. Einige Regeln gelten für mehrere Gruppen, worauf hingewiesen wird. Die Strafen für das Brechen einer Regel hängen von der Regel und der Schwere des Vergehens ab. Strafen können Warnungen,  Degradierung oder sogar einen Ausschluss aus PBST sein. 
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a>
</center>