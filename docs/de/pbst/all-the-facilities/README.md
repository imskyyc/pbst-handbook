# Alle Anlagen

::: danger Bitte beachten: 
Dieser Teil des Handbuchs **könnte** noch einige Änderungen an den Beschreibungen bekommen. Zum Beispiel Standorte für Uniformspender 
:::

## Pinewood Computer Core (PBCC)
Erforsche eine geheime unterirdische Einrichtung, die kurz vor dem Explodieren steht! Deine Mission ist es, einen Supercomputer vor der Überhitzung zu schützen. Erkunde jede Ecke der Einrichtung, überlebe Katastrophen, drücke jeden Knopf, entdecke Geheimnisse und vielleicht schaffst du es lebend raus!

📙 Dieses Spiel wurde im dem Buch "Roblox Top Adventure Games" erwähnt, also schaut es euch an!

- Klicke auf das Symbol unten, um dich Pinewood Builders anzuschließen und zusätzliche Boni zu bekommen!

- Helfer + Credits: Insbesondere Irreflexive, Lenemar und Csdi. SigmaTech, Terrariabat, TheGreatOmni, Unsayableorc für die Hilfe bei Waffen und anderen Gegenständen. Vielen Dank an Jay Cosmic für die Erlaubnis, deinen Song zu verwenden.

## Pinewood Space Shuttle Advantage (PBSSA)
Starte und fliege mit dem Space Shuttle Advantage in den Weltraum! Schließe dann deine Mission ab, den Sateliten in die Umlaufbahn zu bringen und kehre mit deinem Team zur Erde zurück, erreicht die Raumstation, den Mond oder andere Planeten, und dann mutig in eine außerirdische Heimatwelt um außerirdische Waffen zu entdecken!

Project Advantage, unser Einsatz, ein Netzwerk miteinander verbundener Lasersatelliten zu starten! Dieses Spiel wurde in dem Roblox Spotlight Blog und auf der 2013 Londoner Bloxcon vorgestellt. Herausgegeben an die Öffentlichkeit am 28.05.2012.

- Klicke auf das Symbol unten, um dich Pinewood Builders anzuschließen und zusätzliche Boni zu bekommen!

- Helfer + Credits: RedDwarfIV, Irreflexive, Lenemar und Csdi für die Hilfe mit den Waffen

## Pinewood Research Facility (PBRF)
Willkommen in Pinewoods Forschungsanlage, welche sich in einem geheimen Teil der Sahara und über dem Pinewood Computer Core befindet. Es dient als Verteiler zu dem primären Anlagen und Spielen die den Gebäudekomplex in der Sahara bilden - Hochmoderne Megastrukturen und Startrampen für Raketen, sowie die Heimat des Supercomputer-Hauptrechners und Sattelitenkommunikation für unsere Raumfahrtprojekte.

Du kannst die Ausstellung besuchen, in welcher wir dir unsere frühere Technologie und Kreationen zeigen. An diesem Ort befindet sich auch die Pinewood Astronauten Trainingszentrifuge, welche unglaublich hohe G Kräfte entwickeln kann, also schnall dich an!

Diese Anlage wurde zum ersten Mal 2010 nach der Stilllegung der ursprünglichen 2008 Pinewood Labs, der 2009 Research Facility und der Anlagen um das 2009er Hauptquartier geplant. Alle Bereiche, die noch im Bau sind, werden mit einer weißen Wand blockiert.

## Pinewood Hauptquartier (PBHQ)
Willkommen im Hauptquartier der Gruppe. Erstmals im Jahr 2011 gebaut, war dies der Haupttreffpunkt und das Hauptquartier für Pinewood Builders in Tokio, Japan; wir heißen alle Besucher Herzlich willkommen.

Dieses Spiel wurde im Roblox Blog 2013 vorgestellt, in "[Six ROBLOX Places that make You Say "Whoa"](https://blog.roblox.com/2013/06/six-roblox-places-thatll-make-you-say-whoa/)".

## Pinewood Builders Data Storage Facility (PBDSF)
In Wyoming speichert dieses Rechenzentrum Punkte und andere Informationen für die Abteilungen von Pinewood Builders.

Irreflexive - Scripter der Datenbank Csdi - Hauptbauer / UI Design spyagent388 - Assisentsbauer SADENNING - Kleinere Bauarbeiten, Ideen

## PBST Training Facility (PBSTTF)
Projekt CRATER 2015, auch bekannt als die offizielle PBST Training Facility befindet sich direkt außerhalb der Pinewood Research Facility in der Sahara. Der Krater beherbergt eine Vielzahl von Aktivitäten und Szenarien zum traininieren, sowie Gladiatorenarenen um die erfahrenen Spieler herauszufiltern.

Das Klima wird durch einen riesigen klimatisierten Bio-Dome über dem Krater erhalten und der Zugang erfolgt über ein modernes Einschienenbahn-System. Die PBST Schießanlage ist hier auch zu finden.

## PBST Activity Center (PBSTAC)
Brandneue Training, Selbsttraining und Patrouillenzone! Diese Einrichtung enthält das Fünffache der Aktivitäten des ehemaligen "Project CRATER" und viele weitere Aktivitäten im Simulationsraum. Schließen Sie sich PBST an, um Zugang zu der Anlage zu erhalten und großartige Trainings zu besuchen!

Offiziell am 12. Juni 2018 an die Öffentlichkeit gebracht.

## PBST Hub (PBSTH)
Dieser Hub dient als Teleportationsplatz zu allen PBST-Spielen, Anlagen zum patroullieren und zu anderen Spielen! Sie dient auch als zentraler Informationsort für neue Mitglieder, um sich über PBST zu informieren!
