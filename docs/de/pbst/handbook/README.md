
# PBST Handbuch
## Uniform
Die offiziellen PBST-Uniformen finden Sie im Shop [hier](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Auch wenn du keine Robux hast, sind die meisten Anlagen mit Uniform-Gebern ausgestattet, um sie dort zu holen. Es wird trotzdem empfohlen, der Bequemlichkeit halber, eine Uniform zu kaufen.

Du musst beim Patrouillieren einer Pinewood Anlage eine offizielle PBST-Uniform tragen.

**Standorte der Uniform Geber in den Einrichtungen**:
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) Vom Hauptspawn durch die Lobby, den Fahrstuhl hoch, (folge der "Security Sector + Cafe" Linie) und dann im PBST-Raum an der rechten Seite.
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility) Vom Hauptspawn in den Raum mit der Bezeichnung „PB SEC“, dann zu dem Flur zwischen dem Baton-Geber und der Kaffeemaschine, im Umkleideraum auf der linken Seite.
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ) 4. Stock, von den Aufzügen zu den Glastüren links, dann in die blauen Türen auf der linken Seite.
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility) Auf der rechten Seite des Spawngebäudes
* [PBST Aktivitätszentrum:](https://www.roblox.com/games/1564828419/PBST-Activity-Center) In dem Flur der Lobby

Das Tragen einer PET-Uniform ist nicht gestattet, die einzige Ausnahme ist in PBCC, wenn die Temperatur es unmöglich macht, eine normale Strahlenschutzausrüstung zu tragen und eine PET Schutzausrüstung die einzige Möglichkeit ist, in den Kern zu gelangen (3000C+). Sobald die Kern-Temperatur wieder eine normale Schutzausrüstung erlaubt (<3000C) musst du diese wieder anziehen.

## Im Dienst
Du musst eine offizielle PBST-Uniform tragen, um als im Dienst angesehen zu werden. Dein Rangzeichen muss ebenfalls an und auf "Security" gesetzt sein (der Standard), wenn du allerdings nur das Rangabzeichen anhast, bist du nicht im Dienst.

Wenn dieses auf **TMS** oder **PET** gesetzt ist, führe bitte den Befehl `!setgroup PBST` aus

Als PBST Mitglied im Dienst bist du verpflichtet die Anlage vor jeglicher Bedrohung zu schützen. Wenn du evakuieren musst, versuche dein Bestes, den anderen zu helfen.

Um außer Dienst zu gehen, musst du den Sicherheitsraum betreten und deine Uniform und deine PBST-Waffen entfernen. Setze dich selbst zurück falls nötig. Es wird dringend empfohlen, dass du auch dein Security Rangabzeichen entfernst, indem Sie den Befehl “`!setgroup [PBST/TMS/PET/Group Name]`", um zu einer neutralen Gruppe wie PET, PB oder einer anderen Pinewood Gruppe zu wechseln, oder benutze “`!ranktag off`” Befehl, um den Rangabzeichen vollständig auszuschalten.

## Pinewood Computer Core (PBCC)
Der [Pinewood Computer Core](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) ist der wichtigste Ort zu patrouillieren. Dein Hauptziel hier ist zu verhindern, dass der Kern schmilzt oder friert. Die folgende Tabelle zeigt Ihnen, wie unterschiedliche Einstellungen die Temperatur beeinflussen. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228" />

PBCC enthält auch verschiedene „Katastrophen“, von denen einige PBST benötigen, um die Besucher zu evakuieren. Im Falle einer Plasma-Welle, eines Erdbebens oder einer "Strahlenflut", bringe die Menschen, die in Gefahr sind, in Sicherheit.

**Zusätzliche Info**: Um die TPS (Temperaturänderung pro Sekunde) zu berechnen, habe ich einen Rechner hinzugefügt: <Calculator />

## Waffennutzung
Jedes PBST-Mitglied hat Zugang zum Baton, und höhere Ränge erhalten zusätzliche Waffen in blauer Farbe. Stelle sicher, dass du deine Uniform anhast, wenn du deine Waffen nimmst, da du sonst als außer Dienst betrachtet wirst und du keine PBST Waffen außer Dienst benutzen darfst. **PBST-Waffen dürfen niemals verwendet werden, um den Kern zu schmelzen oder einzufrieren.**

Du musst **zwei** Warnungen an andere geben, bevor du sie töten darfst. Wenn ein Besucher nach drei Warnungen getötet wurde und trotzdem zurückkehrt, kannst du diese Person *ohne Vorwarnung* töten. PBST-Mitglieder, welche im Dienst sind, müssen diese Regeln auch befolgen, wenn sie nicht-PBST-Waffen, wie die vom OP Weapons Gamepass oder die Pistolen, die von PBCC-Credits gekauft oder zufällig gespawnt werden, benutzen.

Im Notfall kann ein Tier 4 oder höher Ihnen die Erlaubnis geben, einen Raum auf **nur PBST** zu beschränken. In diesem Fall kannst du jeden töten, der versucht hereinzukommen, und darfst nur *PBST im Dienst* hereinlassen.

Wenn du außer Dienst bist, darfst du nur Nicht-PBST-Waffen verwenden, du hast in der Benutzung allerdings mehr Freiheiten. Du kannst zum Beispiel Räume ohne Erlaubnis einschränken, obwohl übermäßige Raumbeschränkungen als "mass random killing" (übermäßiges töten) gelten könnte.

Mass Random Killing (Übermäßiges töten | MRK) und Spawn Killing (SK) sind immer verboten, benutze den`!call` Befehl um PIA zu rufen, wenn jemand dies tut.

## The Mayhem Syndicate (TMS)
The Mayhem Syndicate (**TMS**) ist das Gegenteil von PBST. Während wir den Kern schützen wollen, wollen sie ihn schmelzen oder einfrieren. Du kannst sie am roten Rangabzeichen erkennen. Sie nehmen oft an "Raids" (Überfällen) teil,  wenn dies passiert, solltest du nach Verstärkung rufen.

Der einzige Ort, an dem TMS nicht getötet werden darf (abgesehen von Spawn), ist die TMS Basis in der Nähe der Güterzüge. Du musst TMS eine faire Chance geben, ihre Waffen zu holen, da sie dasselbe für PBST-Loadouts tun. Campe nicht bei den TMS Loadouts.

Wie bei allen bewaffneten Gegnern, ist es dir erlaubt dich zu verteidigen, wenn TMS dich angreift, selbst wenn diese auf dem Weg zu ihrem Loadout zu sein scheinen.

Wenn du während des TMS Überfalles außer Dienst bist und an ihm teilnehmen möchtest, musst du dir eine Seite aussuchen und auf dieser Seite kämpfen, bis der Überfall beendet ist. **Ändern Sie die Seite nicht mitten in einem Überfall!** Dies gilt auch, wenn Sie neutral oder außer Dienst sind und sich trotzdem dafür entscheiden teilzunehmen. Beachte, dass du wenn du dich auf entweder die Seite von TMS oder PBST während des Raids stellst, dass du auf KoS (Kill on Sight, bei Sicht töten) von der anderen Seite gesetzt werden könntest.

## Töten auf Sicht (KoS)
Jedes Mitglied von TMS sollte auf Sicht getötet werden.

Mutanten sind ebenfalls KoS. **PBST-Mitglieder dürfen während sie im Dienst sind nicht zu Mutanten werden.**

PBST im Dienst darf keine andere Person auf KoS setzen, es sei denn, es wurde von einem Tier 4 oder höher eine Erlaubnis erteilt.

## Training
::: warning Warnung:
Anweisungen während des Trainings werden in Englisch gegeben, daher muss man Englisch verstehen. 
:::

Tier 4 und höher können Trainings veranstalten, bei dem du [Punkte ]((../ranks-and-ranking-up/)) verdienen kannst. Den Zeitplan dafür findest du in der [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Trainingsregeln**:
* Höre auf den Host und folge seinen Anweisungen.
* Nimm keine Ausrüstung, der Host gibt dir das, was du für die Aktivitäten brauchst.
* Trage im Training immer eine PBST-Uniform.
* Trainer können die Berechtigung zum Sprechen (PTS) aktivieren, welches bedeutet, dass du bestraft wirst, wenn du redest. Sage "`;pts request`", um Erlaubnis zu bekommen, wenn dies aktiviert ist.
* Einige Trainings sind als Disziplinartraining gekennzeichnet, sie sind strenger und sind stärker auf Disziplin ausgerichtet. Du wirst aus dem Training geworfen, wenn du dich falsch verhältst.
* Höchstens einmal pro Woche wird ein Hardcore Training veranstaltet. In einem solchen Training führt selbst der kleinste Fehler zu einem Rausschmiss.  Wenn du bis zum Ende „überlebst“ , kannst du bis zu 20 Punkte verdienen.

Weitere Informationen über Punkte findest du [hier](../ranks-and-ranking-up/)

## Ränge
Du wirst den nächsten Rang erhalten, sobald du die erforderliche Anzahl an Punkten erreicht hast. Mit deinem neuen Rang kommt eine neue Ausrüstung in Pinewood-Anlagen: Tier 1 gibt dir einen stärkeren Schlagstock, ein Einsatzschild, einen Elektroschocker und eine PBST Pistole. Mit Tier 2 bekommst du alle eben genannten und ein langsames, starkes Gewehr. Mit Tier 3 bekommst du alle eben genannten und ein Maschinenpistole. Diese Waffen finden Sie im Sicherheitsraum in Pinewood-Anlagen, und Cadets sind dort drin nicht erlaubt.

::: tip Kürzliche Änderung! 
Seit dem `28. Februar 2020` werden Tier **1** Prüfungen durchgeführt. Wenn du von `Kadett` zu `Tier 1` aufsteigen willst musst du an einer Prüfung teilnehmen.

Du kannst [hier](../ranks-and-ranking-up/#tier-evaluations) mehr darüber nachlesen . 
:::

Sobald man zu einem Tier befördert wird, wird erwartet, dass man ein Vorbild für Kadetten ist. Verletzung der Regeln können zu größeren Bestrafungen führen. Insbesondere Tier 3 Mitglieder sollten die perfekte Darstellung von PBST sein. **Du wurdest gewarnt.**

Tiers haben Zugriff auf den Befehl `!call PBST`, den sie für einen bevorstehenden Melt- oder Freezedown oder füe einen Raider verwenden können. Dies ist verantwortungsbewusst zu verwenden.

Als Tier kannst du ausgewählt werden,  bei einem Training den Host zu unterstützen, weshalb du temporäre Administratorbefugnisse erhälst, die ausschließlich für dieses eine Training bestimmt sind. Der Missbrauch dieser Befugnisse wird zu einer schweren Bestrafung führen.

## Tier 4 (aka Special Defense, SD)
Tier 3's mit 800 Punkten sind quallifiziert, eine SD Prüfung abzulegen, um Tier 4 zu werden. Wenn du ausgewählt wirst, musst du ein  PBST-Training veranstalten und deine Leistung wird genau beobachtet. Wenn du bestehst, erhältst du den Titel „Passed SD Eval“ bis die Trainer dich zu Tier 4 befördern. Discord zu haben ist dafür erforderlich.

Als Tier 4 erhälst du die Möglichkeit, KoS in Pinewood-Anlagen zu setzen und du musst keine Uniform mehr tragen. Du erhälst auch Kronos Mod in den Trainingsanlagen, welches verantwortungsvoll genutzt werden soll.

Tier 4's können Trainings mit Erlaubnis eines Trainers veranstalten. Sie dürfen bis zu 6 mal in der Woche ein Training veranstalten (Mega Trainings nicht inbegriffen), allerdings maximal 2 pro Tag. Tier 4's dürfen nicht länger als 3 Tage im Voraus ein Training beantragen. Es muss auch eine 2-Stunden-Lücke zwischen dem Ende eines Trainings und dem Beginn eines anderen geben.

## Trainer
Um Trainer zu werden, müssen alle derzeitigen Trainer über deine Beförderung abstimmen. Nur die Tier 4's sind für diese Beförderung berechtigt.

Trainer können ohne Einschränkungen Trainings veranstalten, obwohl die 2-Stunden-Regel weiterhin gilt. Trainer sind auch für einige der administrativen Aufgaben von PBST verantwortlich, wie z. B. Punkte in die Datenbank eintragen und Befördern. Bitten frage die Trainer nicht, deine Punkte einzutragen oder dich zu Befördern, habe Geduld.

Trainer, die Pinewood-Anlagen patrouillieren, sind von den meisten Regeln im Handbuch befreit, obwohl die folgenden Erinnerungen auch für sie gelten.

# Erinnerungen an alle PBST-Mitglieder
  * Bitte halte dich die ganze Zeit an die [ROBLOX Gemeinschaftsregeln](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules).
  * Sei respektvoll gegenüber deinen Mitspielern, handel nicht so, als wärst du der Chef des Spiels.
  * Folgen Sie den Anweisungen von jedem mit einem höheren Rang.

::: warning ACHTUNG: 
Diese Regeln können jederzeit geändert werden. Bitte überprüfe daher ab und zu ob das Handbuch aktualisiert worden ist.
:::
