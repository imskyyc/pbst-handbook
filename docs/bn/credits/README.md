---
sidebar: আউত
---

# চ্রেদিতস ওহেরে থেয়'রে দুএ
ওএ ওওউলদ লিকে ত ফল্লিং পেওপ্লে ফর হেল্পিং উস ওইত্য থে হান্দবুক

## বানকেন্দ + ডেভেলপমেন্ত
* শতেফান / সুপেরস্তেফান৪

## অনুবাদ:
;;;তিপ ট্রান্সলাতিওন্স আরে চুররেন্তল্য বেইং মাদে ডুএ ত থে ফাচত থাত ত্রান্সলাতিওন্স নেএদ ত বে মানুয়াল্ল্য আপ্প্রভেদ This section has been **TEMP** removed. It will return as soon as the first language reaches 100% completeness. If you would like to help, please go to [this](https://translate.pinewood-builders.ga) website. And submit a language to use. (Contact me on [this email](mailto:stefano@stefanocoding.me), to add your language) :::


## Proofreading
### HoS
* Csd | Csdi

### PIA
* Omni | TheGreatOmni
### Trainers
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
