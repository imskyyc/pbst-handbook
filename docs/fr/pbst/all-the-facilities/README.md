# Toutes les installations

:::danger Veuillez noter : Cette partie du manuel **peut** apporter quelques modifications à leurs descriptions. Comme pour les emplacements des distributeurs d'uniforme. :::

## Noyau Ordinateur Pinewood (PBCC)
Explorez une installation souterraine secrète qui est sur le point d'exploser ! Votre mission est d'empêcher un super-ordinateur de surchauffer. Explorez chaque coin de l'installation, survivez des catastrophes, appuyez sur chaque bouton, découvrez des secrets, et vous pourrez peut-être en sortir vivant !

📙 Ce jeu a été mis en vedette dans le livre Roblox Top Adventure Games, alors n’hésite pas à le visiter !

- Cliquez sur l'icône ci-dessous pour rejoindre Pinewood Builders pour des avantages supplémentaires!

- Aides + Crédits: Irreflexive, Lenemar et Csdi en particulier. SigmaTech, Terrariabat, TheGreatOmni, Unsayableorc pour l'aide avec les armes et les objets. Remerciement à Jay Cosmic pour la permission d'utiliser votre chanson.

## Navette spatiale Advantage de Pinewood
Lancez et montez de la navette spatiale Advantage dans l'espace ! Terminez ensuite votre mission de déploiement de satellites en orbite et retournez sur Terre avec votre équipe, atteignez la Station Spatiale, la Lune et les autres planètes, puis partez courageusement vers un monde d'origine extraterrestre pour découvrir des armes extra-terrestres !

Projet Advantage, nos efforts pour lancer un réseau de satellites lasers interconnectés ! Ce jeu a été mis en vedette dans le blog de Roblox Spotlight et le London Bloxcon. Publié au public le 28 mai 2012

- Cliquez sur l'icône ci-dessous pour rejoindre Pinewood Builders pour des avantages supplémentaires!

- Aide + Crédits: RedDwarfIV, Irreflexive, Lenemar, Csdi pour l'aide avec les armes

## Centre de Recherche de Pinewood (PBRF)
Bienvenue dans le centre de recherche de Pinewood, situé dans une partie secrète du désert du Sahara et au-dessus du Noyau Ordinateur Pinewood souterrain. Il sert de point de convergence entre nos installations principales et nos jeux qui composent le complexe du Sahara, nos méga-structures dernier cri et sites de lancement de navettes ainsi que l'emplacement de notre super-ordinateur principal et la communication par satellite pour nos projets spatiaux.

Vous pouvez venir visiter l'exposition où nous vous montrons nos technologies et nos créations passées. Cet endroit est également doté de la centrifuge d'astronaute Pinewood capable d'aller à une vitesse incroyablement élevée, alors accrochez vous.

Cette installation a été proposée pour la première fois en 2010, après avoir mis hors service les laboratoires Pinewood 2008 originaux, le centre de recherche 2009 et les installations du QG 2009. Toutes les zones encore en construction seront bloquées par un mur blanc.

## Quartier général de Pinewood (PBHQ)
Bienvenue au siège social de notre groupe. Construit pour la première fois en 2011, ce fut le principal lieu de rencontre et le siège de Pinewood Builders, situé à Tokyo, au Japon; nous accueillons tous les visiteurs.

Ce jeu a été présenté dans le Roblox Blog 2013, dans "[Six endroits ROBLOX qui vous feront dire "Whoa"](https://blog.roblox.com/2013/06/six-roblox-places-thatll-make-you-say-whoa/)".

## Installations de stockage de données de Pinewood Builders (PBDSF)
Situé dans le Wyoming, ce centre de données dédié stocke des points et d'autres informations pour les divisions de Pinewood Builders.

Irreflexive - Scripteur de la base de données Csdi - Constructeur principal / UI Design spyagent388 - Constructeur assistant SADENNING - Bâtiment mineur, Idées

## Centre d'entrainement PBST (PBSTTF)
Le projet CRATER 2015, aussi connu sous le nom de "Centre d'entrainement officiel de PBST" est situé juste à l'extérieur de l'installation de recherche de Pinewood dans le Sahara. Le cratère d'impact accueille une grande variété d'activités, d'entraînement et de scénarios, ainsi que des arènes de gladiateurs pour séparer les compétents.

Le climat est maintenu grâce à un énorme bio-dôme climatisé au-dessus du cratère et le centre est accessible par un système de monorail à la pointe de la technologie. Le dôme de tir de PBST se trouve également ici.

## Centre d'activité PBST (PBSTAC)
De nouveaux entraînements, auto-entraînements et zone de patrouille ! Cette installation contient cinq fois plus d'activités utilisées que dans l'ancien « Projet CRATER » et de nombreuses activités dans la salle de simulation. Rejoignez PBST pour accéder à l'installation et assister à des entraînements géniaux !

Publié officiellement au public depuis le 12 juin 2018.

## Hub PBST (PBSTH)
Ce hub sert de lieu de téléportation à tous les jeux PBST, les installations de patrouille et autres jeux! Il sert également de centre d’information pour les nouveaux membres pour en apprendre sur la PBST!
