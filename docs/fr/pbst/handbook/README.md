
# Manuel de la PBST
## Uniformes
Les uniformes officiels PBST peuvent être trouvés dans la boutique [ici](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Si vous n'avez pas de Robux, la plupart des installations sont équipées de distributeurs d'uniformes pour vous faire avancer. Il est cependant recommandé d'acheter un uniforme pour votre commodité.

Vous devez porter un uniforme officiel de PBST lorsque vous patrouillez dans une installation Pinewood.

**Emplacements des distributeurs d'uniformes dans les installations**:
* [Ordinateur Noyau Pinewood](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) : Depuis le spawn principal dans le lobby, monter avec l'ascenseur (suivre 'Secteur Sécurité + Cafe)', dans la salle PBST à droite
* [Installations de recherche de Pinewood](https://www.roblox.com/games/7692456/Pinewood-Research-Facility) : Du spawn principal vers la pièce marquée 'PB SEC', le couloir entre le distributeur de matraque et la machine a café, première salle à gauche.
* [Quartier général de Pinewood](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ) : 4ème étage, partez des ascenseurs  jusqu'aux portes vitrées sur la gauche, et passez les portes bleues sur la gauche.
* [Centre d'entrainement PBST](https://www.roblox.com/games/298521066/PBST-Training-Facility) : À droite, dans le bâtiment d'apparition.
* [Centre d'activités PBST](https://www.roblox.com/games/1564828419/PBST-Activity-Center) : Dans le couloir principal du lobby.

Porter un uniforme PET n'est pas autorisé, la seule exception est au PBCC lorsque la température du noyau rend impossible le port d'un costume standard et une combinaison PET Hazmat est la seule façon d'entrer dans le noyau (3000C+). Une fois que la température du noyau permet à nouveau de porter une combinaison standard (3000C-), vous devez utiliser cette combinaison.

## En service
Vous devez porter un uniforme officiel PBST pour être considéré comme en service. Votre tag de rang doit également être activé et défini sur "Security" (ce qui est la valeur par défaut), mais avoir juste le tag de rang ne vous met pas en service.

Si votre rang est mis à **TMS** ou **PET**, exécutez la commande `!setgroup PBST`

En tant qu'officier en service de PBST, vous devez protéger l'installation contre toutes menaces qui pourraient se présenter. Lorsque vous devez évacuer, faites tout ce qui est en votre pouvoir pour aider les autres.

Pour passer en hors-service, vous devez vous rendre dans la salle de sécurité et retirer votre uniforme et vos armes PBST. Réinitialiser votre personnage si nécessaire. Il est fortement recommandé de supprimer également votre tag de rang de "sécurity" en utilisant la commande « `!setgroup [PBST/TMS/PET/Group Name]`» pour passer à un groupe neutre comme PET, PB ou tout autre groupe Pinewood dans lequel vous vous trouvez, ou utilisez la commande « `!ranktag off` » pour désactiver complètement votre rang qui se situe au dessus de votre tête.

## Le Noyau Ordinateur Pinewood (PBCC)
Le [Noyau Ordinateur Pinewood](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) est l’endroit le plus important pour patrouiller. Votre objectif principal est d'empêcher le noyau de fondre ou de geler. Le graphique suivant vous montre comment les différents paramètres affectent la température. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228" />

PBCC propose également divers « désastres », dont certains ont besoin de la PBST pour évacuer les visiteurs. En cas d'apparition de plasma, de tremblement de terre ou d'inondation de radiation, les personnes en danger doivent être mises en sécurité.

**Infos supplémentaires** : Pour calculer la TPS (température par seconde) j'ai ajouté une calculatrice : <Calculator />

## Utilisation des armes
Chaque membre PBST a accès a la matraque standard, et les Tiers reçoivent des armes supplémentaires provenant des arsenaux bleus. Assurez-vous d'avoir votre uniforme lorsque vous prenez ces armes, parce que vous êtes considéré comme hors service sans uniforme et que vous ne pouvez pas utiliser des armes PBST en étant hors service. **Les armes PBST ne doivent jamais être utilisées pour causer une fonte ou un gel du noyau.**

Vous devez donner **deux** avertissements aux personnes avant de pouvoir les tuer. Si un visiteur a été tué après trois avertissements et reviens toujours, vous pouvez tuer cette personne *sans avertissement*. Les membres de la PBST en service doivent également respecter ces règles lorsqu'ils utilisent des armes non-PBST comme le passe de jeu "Armes OP", ou le pistolet acquis à partir de crédits PBCC ou généré aléatoirement

En cas d'urgence, un Tier 4 ou supérieur peut vous accorder la permission de restreindre une salle à la **PBST seulement**. Dans ce cas, vous pouvez tuer toute personne qui essaye d'entrer et laisser seulement des membres *PBST* en service.

En cas de non-service, vous ne devez utiliser que des armes autres que celle de PBST, bien que vous ayez plus de liberté avec votre utilisation. Vous pouvez, par exemple, restreindre des zones sans avoir besoin une permission, bien que les restrictions excessives de zone puissent être considérées comme un "massacre aléatoire de masse".

Le massacre aléatoire de masse et le massacre de spawn sont toujours interdits, utilisez la commande ` !call` permet d'appeler un PIA à votre aide si quelqu'un qui tue sans raison aléatoirement ou si quelqu'un tue au spawn .

## Le Syndicat Mayhem (TMS)
Le Syndicat Mayhem (**TMS**) est le contraire de PBST. Si nous avons l'intention de protéger le noyau, ils ont l'intention de le faire fondre ou de le congeler. Vous pouvez les reconnaître par le tag de rang rouge. Ils se joignent souvent à une partie dans une attaque, et quand cela se produit, il est conseillé de faire un appel pour des renforts.

Le seul endroit où les TMS ne doivent pas être tué (à part à la zone d'apparation) est aux arsenaux TMS près des trains de marchandises. Vous devez laisser aux TMS une chance de prendre leurs arsenaux, car ils feront la même chose pour les arsenaux PBST. Ne campez pas aux arsenaux TMS.

Comme pour tous les ennemis armés, vous êtes autorisé à vous défendre si un TMS vous attaque, même s'ils semblent être en route pour obtenir leurs équipements.

Si vous êtes hors service lorsque des TMS commencent à attaquer et que vous voulez participer, vous devez choisir un camp et vous battre de ce côté jusqu'à la fin de l'attaque. **Ne changez pas de camp en pleine attaque.** Cela s'applique également si vous êtes neutre ou hors service et décidez de participer de toute façon. Faites attention, si vous choisissez une équipe dans un raid, cela pourrai vous mettre en KoS (Tuer à vue) dans l'autre équipe.

## Tuer à vue (KoS)
Tout membre de TMS doit être tué à vue.

Les mutants (zombies) sont également en KoS. **Les membres de PBST ne doivent pas devenir mutants en étant en service.**

Un membre PBST en service ne peut mettre personne en KoS à moins que l'autorisation n'ait été accordée par un Tier 4+.

## Les Entraînement
::: REMARQUE : Les instructions d'un entrainement sont données en anglais, la compréhension de l'anglais est donc requise. :::

Les Tiers 4+ peuvent organisés un entraînement où vous pouvez gagner des [points]((../ranks-and-ranking-up/)). L'emploi du temps des entrainements peut être trouvé dans [l'Installation de stockage de données de Pinewood Builders](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility) (PBDSF).

**Règles d'entraînement** :
* Écoutez l'hôte (personne qui est sur le podium et qui vous entraîne pendant 1 heure) et vous devez suivre ses ordres.
* Ne prenez pas vos équipements, l'hôte vous donnera ce dont vous avez besoin dans chaque activité.
* Portez toujours un uniforme PBST lors d'un entraînement.
* Les entraîneurs peuvent activer la permission de parler (PTS), ce qui signifie que parler dans le chat sera suivi d'une sanction. Ecrivez '`;pts request`' pour obtenir la permission de parler si c'est activé (seulement pour des cas en particuliers [Exemples : AFK - problème(s) de compréhension / problème(s) en général]).
* Certains entraînements seront des entraînement disciplinaire, qui seront plus strictes et concentrés sur la discipline. Vous serez expulser de l'entrainement si vous vous comportez mal.
* Une fois par semaine au maximum, un entraînement difficile sera hébergé. Dans cet entrainement, même la moindre erreur vous fera expulser. Si vous « survivez » jusqu’à la fin, vous pouvez gagner jusqu’à 20 points.

Plus d'informations à propos des points seront trouvables [ici](../ranks-and-ranking-up/).

## Les Tiers
Vous recevrez une promotion une fois que vous aurez atteint le nombre de points requis. Avec votre nouveau rang vient un nouvel arsenal dans les installations de Pinewood : le Tier 1 vous donne une matraque plus puissante, un bouclier anti-émeute, un taser et un pistolet PBST. Avec le Tier 2, vous recevez tous les anciens plus un fusil. Au Tier 3, vous recevez tous les anciens plus une mitraillette. Ces armes peuvent être trouvées dans la salle de sécurité des installations de Pinewood et les cadets ne sont pas autorisés à y entrer.

::: astuce Changement Récent! À partir du `02/28/2020`. Les évaluations des Tiers **1** sont maintenant en place. Si vous voulez aller de `Cadet` -> `Tier 1`. Vous devez compléter une évaluation.

Vous pouvez en savoir plus [ici](../ranks-and-ranking-up/#tier-evaluations). :::

Une fois que vous êtes promu au Tier supérieur, vous êtes censé être un modèle pour les cadets. Toute violation des règles peut entraîner une sanction plus grande. Il faut s’attendre en particulier à ce que les Tier 3 soient la représentation parfaite de PBST à son meilleur. **Vous avez été averti.**

Les Tiers ont accès à la commande `!call PBST`, qu'ils peuvent utiliser pour une fusion imminente, un gel ou une attaque. Cela doit être utilisé judicieusement.

En tant que Tier, vous pouvez être sélectionné pour aider à l'entrainement, où vous recevrez des pouvoirs d'administration temporaires qui sont strictement pour cet entrainement spécifique. Les abus de ces pouvoirs se traduiront par de sévères sanctions.

## Tier 4 (Défense spéciale, SD)
Les Tiers 3 qui atteignent 800 points sont admissibles à une évaluation pour devenir Tier 4. Si vous êtes choisi, il vous sera demandé d'organiser un entrainement PBST et votre performance sera surveillée de près. Si vous passez, le titre « Passed SD Eval » vous sera donnée jusqu’à ce que les entraineurs choisissent de vous promouvoir Tier4. Avoir Discord est requis.

Comme le Tier 4, vous recevez la possibilité de placer un KoS dans des installations Pinewood et vous n'êtes plus obligé de porter un uniforme. Vous recevrez également le mode Kronos dans les installations d'entrainement de Pinewood, qui doivent être utilisées de manière responsable.

Les Tiers 4 peuvent organiser un entrainement avec la permission d’un entraîneur. Ils peuvent héberger 6 fois par semaine (les Mega ne sont pas inclus), avec un maximum de 2 par jour. Les Tiers 4 ne peuvent pas demander d'organiser un entrainement plus de 3 jours à l'avance. Il doit également y avoir un écart de deux heures minimum entre la fin d'un entrainement et le début d'un autre.

## Les Entraineurs
Pour devenir un entraîneur, tous les entraineurs actuels doivent voter sur votre promotion. Seuls les Tiers 4 sont admissibles à cette promotion.

Les entraineurs peuvent accueillir des entrainements sans restrictions, mais la règle de 2 heures s'applique toujours. Les entraineurs sont également responsables de certaines tâches administratives plus importantes de PBST, telles que l’enregistrement des points et les promotions. Ne demandez pas aux entraineurs d'enregistrer vos points ou de vous promouvoir. Soyez patient.

Les entraineurs qui patrouillent dans les installations de Pinewood sont exemptés de la plupart des règles du manuel, bien que les rappels suivants s'appliquent également à eux.

# Rappels à tous les membres PBST
  * Veuillez vous conformer aux règles de la communauté [ROBLOX](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) tout le temps.
  * Soyez respectueux envers les autres joueurs, n’agissez pas comme si vous étiez le patron du jeu.
  * Suivez les ordres de toute personne plus haut gradé que vous.

::: avertissement REMARQUE : Ces règles peuvent être modifiées à tout moment. Veuillez s'il vous plaît vérifier de temps en temps pour toutes mise à jour sur le manuel. :::
