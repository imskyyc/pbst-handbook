# Les Commandes de Tier 4 et d'Entraîneur

:::Attention. Ces commandes sont utilisées les entrainements "la plupart" du temps. Beaucoup de commandes affichées sur cette page seront utilisées par les assistants, les Tiers 4 et les entraineurs. 0/> Chaque installation PB a un système d'autorisation, donc l'utilisation de ces commandes en tant qu'un -T4 ne sera pas possible.

En hébergeant un entrainement, ou en y participant en tant qu'assistant. La formation est donnée en **ANGLAIS**. Assurez-vous donc de parler couramment anglais, ou du moins, que l'on vous comprenne. :::

:::Danger. Ne faîtes pas de copier/coller Veuillez noter que ces commandes sont des **EXAMPLES**, vous devez changer ensuite à votre entrainement la chose que vous avez besoin de faire . 1/> Assurez-vous d'avoir testé vos commandes avant de les utiliser dans un **VRAI** entrainement... :::

## Arguments possibles
- `me` -> Vous même.
- `player-name` -> Un joueur que vous sélectionnez.
- `all` -> Tout le monde sur le serveur (Donc vous aussi même et les observateurs).
- `%team-name` -> Sélection d'une équipe (**Exemple**: `;team radius-20 %Winners`).
- `nonadmins` -> Chaque utilisateur qui n'a pas les permissions d'administration (Souvent tout le monde sous le rang Tier 4). **NOTE** : Cela signifie que les observateurs peuvent également être sélectionnés avec cet argument).
- `radius-<distance>` -> Tout le monde dans la distance que vous spécifiez. (**Exemple** : `;team radius-20 %Winners`)
- `others` -> Ceci sélectionne tout le monde, sauf vous-même (Si vous avez des assistants, ou des observateurs, vous les sélectionnez aussi)

## Quelles commandes sont généralement utilisées dans les formations?
- `;fly | ;god`  
Cette chaîne de commandes vous donne la permission de voler, et vous rend invincible (vous ne pouvez pas prendre de dégâts)
- `;team <argument> <team>`  
Placez un joueur dans une équipe, selon les équipes que vous avez créées avec les commandes ci-dessous
- `;newteam Host/Assistants <color>`  
Lorsque vous hébergez une formation, cette commande donne normalement au joueur que vous définissez l'équipe Hôte/Assistant.
- `;newteam Observers <color>`  
Créez une équipe pour les observateurs (personnes qui suivent l'entraînement). **NOTE** : Ceux-ci n'auront pas de points
- `;newteam Winners <color>`  
Une équipe pour définir les gagnants d'une partie, et leur donner des victoires (Pour que vous ne donniez pas à tout le monde une victoire)
- `;newteam Security <color>`  
Il s'agit de l'équipe par défaut, où tout le monde devrait être (à l'exception de l'hôte, des observateurs et des assistants) ![img](https://i.imgur.com/vLwSyFK.png)

- `;addstat Wins`  
Ajouter une statistique pour voir qui a gagné des points et qui n'en a pas.
- `;m <message>`  
C'est une annonce que vous pouvez voir à l'écran, avec la ligne roblox par défaut. ![img](https://i.imgur.com/PcjEr9v.png)
- `;n <message>`  
Une petite note que tout le monde voit sur son écran ![img](https://i.imgur.com/rZKvc03.png)
- `;h <message>` ![img](https://i.imgur.com/YXRuuJB.png) Un message envoyé en haut de l'écran de tout le monde
- `;add <argument> Wins 1`  
Donne une victoire à quelqu'un ou à une équipe (les joueurs n'ont pas besoin d'un %, mais les équipes en ont besoin).
- `;team radius-<distance> <Team>`  
Permet de changer d'équipes les personnes autour de vous, c'est très utile pour donner des victoires
- `;give <argument> <tool>`  
Donne aux gens un outil qui se trouve dans la boîte à outils
- `;sword <argument>`  
Une courte commande pour donner des épées aux personnes
- `;removetools <argument>`  
Supprime les outils des personnes sélectionnées
- `;viewtools <argument>`  
Vous permet de voir les outils qu'une personne a sur elle
- `!addalias <!alias> <Command>`  
Utilisez cette commande pour ajouter un alias à votre propre collection (par exemple : **!addalias !shirtme !shirt [146487695](https://www.roblox.com/catalog/146487695/Security-Vest-Specops-PB)**)

## Suggestions d'alias

 1. Ouvrez Kronos en cliquant sur le coin inférieur droit de votre écran (Logo PBST)
 2. Ouvrez l'onglet "Alias"
 3. Appuyez sur "Ajouter" en bas de la fenêtre.
 4. Remplissez l'alias que vous souhaitez pour votre commande, dans le champ **Alias:**
 5. Entrez l'une des commandes ici ou l'une de vos propres commandes

**`;assist`**:
- `;team <arg1> Host/Assistant | ;fly <arg1> | ;god <arg1> | ;sword <arg1>` Donne des commandes d'assistant et met la personne sur l'équipe Hôte/Assistant. Les entraîneurs peuvent également utiliser des commandes supplémentaires pour améliorer l'alias :
- `;équipe <arg1> Hôte/Assistants | ;admin <arg1> | ; oadout <arg1> Spécial` Donne à la personne des pouvoirs de modérateur temporaire pour l'entrainement et l'arsenal de niveau 4.

**`;observe`**:
- `;team <arg1> Obs | ;fly <arg1> | ;god <arg1>`   
Place quelqu'un dans l'équipe d'observation et lui donne des commandes d'observateur  
![img](https://i.imgur.com/imuV43Y.png)

**`;s2vote`**:
- `;vote nonadmins,-%ob Sword_Fight,Guns,Juggernaut,Bombs 20  What activity shall we do?` Utile pour les hôtes de sélectionner facilement une arène dans le secteur 2.

**`;noreturn`**:
- `;lobbydoors off | ;teleporters off`  
Verrouille le lobby pour que personne ne puisse en sortir

**`;bafk`**:
- `;bring <arg1> | ;afk <arg1>` Apporte la personne et la rend AFK.

**`;flygod`**:
- `;fly <arg1> | ;god <arg1>` Cela permet à l'argument sélectionné de voler et d'être dieu (Invincible).

**`;bjail`**:
- `;bring <arg1> | ;jail <arg1>` Ramène la personne et la met en prison devant vous.

**`;win20`** :
- `;team radius-20 %Winners`

**`;twin`**:
- `;add %Winners Wins 1`

**`;unteamwin`**:
- `;unteam %Winners` Ce processus est utilisé pour donner facilement des victoires à un certain groupe de personnes.

**`;trainingsetup`**:
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on | ;addstat Strikes` (Cette dernière peut également être utilisée avec **`;strike`**) Crée chaque équipe dont vous aurez besoin et donne les statistiques des « Victoires ». Pour que l'alias soit plus avancé, il y en a plusieurs sur cette ligne.


**Crédits à *MarusiFyren* et *EquilibriumCurse* pour m'aider à créer cette page, et marusi pour certaines des captures d'écran utilisées sur cette page.**