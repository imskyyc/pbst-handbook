# Points et classement "Rank up"
## Abréviations
 * ST (pour Self-Training) en traduction Auto-formation.
 * PBST (Pinewood Builders Security Team)

## Comment gagner des points
Les points peuvent être gagnés de plusieurs façons, vous pouvez suivre un [ST](#abbreviations). Être dans un entraînement, ou être dans des événements comme des raids, et autres choses. Selon ce que vous faites, vous obtenez des points en fonction de ce que vous faites. Être dans un raid (côté [PBST](#abbreviations)).

Les formations sont également une option, les formations sont annoncées sur notre mur de groupe. Les entraînement peuvent êtres organisés par :
 * Des entraîneurs
 * Des Tiers 4

Toute personne est en mesure de participer à une formation/entraînement, mais l’organisateur ou le formateur a le droit de choisir qui sera l'assistant, demander au formateur n’aidera pas. Cela ne fera que réduire vos chances de devenir un assistant, plus d’informations sur certaines des règles peuvent être trouvées [ici](../handbook/#training).

Comme dernière option, vous pouvez toujours suivre un [ST](#abbreviations). Ces formations sont conçues spécifiquement pour que les gens gagnent des points d’une manière différente, et pour pratiquer leurs compétences dans une activité spécifique. Plus d'informations seront données [ici](#self-trainings)

## Le "Rank up"

Le classement nécessite des points, qui peuvent être gagnés dans les entraînements, [ST](#abbreviations) et dans les patrouilles. Si vous obtenez le nombre de points requis pour le grade suivant, vous serez promu. Le nombre de points requis pour n’importe quel grade peut être trouvé au spawn au [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center), ~~bien que vous pouvez ignorer la partie des évaluations, ceux-ci sont seulement en place pour devenir Tier 4~~~. Cela a maintenant changé. S'il vous plaît, lisez le message en-dessous...

:::danger Gardez à l'esprit ! Quand vous êtes un `Tier 1+` vous pouvez être puni plus dur pour les erreurs que vous faites, vous êtes censé être un modèle pour tous les cadets et visiteurs des **installations Pinewoods**. Ceci n'a pas d'exeption (sauf indication contraire comme règle uniforme T4) :::

::: avertissement AVERTISSEMENT S’il vous plaît regarder avec les promotions actuelles, il a été annoncé sur le discord de la PBST. Cela indique que le **Tier 1** devra passer une évaluation. Comme le dit ce message : <img class="no-medium-zoom zooming" src="https://i.imgur.com/MxDifjh.png" alt="demo" width="1228" /> :::

## Évaluations des Tiers
Une fois que vous avez atteint 100 points, vous devez participer à une évaluation de Tier si vous voulez obtenir le rang **Tier 1**. Comme pour les entrainements normaux, celles-ci peuvent être trouvées sur [le planning](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Elles seront hébergées régulièrement pour accueillir tous les fuseaux horaires.

Il y a un serveur spécialisé pour les évaluations de Tier, utilisez la commande `!pbstevalserver` pour y aller. Ceci ne fonctionne que si vous avez plus de 100 points.

**Une évaluation de Tier se compose en 2 parties**:
  1. Un quiz et un test sur les compétences de patrouille.
  2. Les règles d'entrainements seront lourdement renforcées dans cet évaluation, ne pas les suivre aboutira à un échec immédiat.

Pendant le quiz, vous recevrez 8 questions sur différents sujets, y compris ce manuel et les règles d'entrainements. Les questions varieront en difficulté, certaines sont faciles mais d'autres nécessitent plus de réflexion. Vous devez marquer au moins 5/8 pour passer. La réponse doit être faite en privé par le biais d'un chat "chuchotement" ou d'un système de message privé envoyé par l'hôte.

Pendant le test de patrouille, vous recevrez des armes de **Tier 1** et serez testé sur des compétences comme vos capacités en combat, de travail d'équipe, et de suivre le manuel correctement. Un certain nombre de Tier 3 travailleront contre vous dans cette partie, car ils essaieront de faire fondre ou geler le noyau.

Une fois que vous aurez réussi l'évaluation de niveau, vous serez promu au niveau **Tier 1**. Avec votre nouveau rang vient un nouvel arsenal dans les installations de Pinewood : le Tier 1  vous donne une matraque plus puissant, un bouclier anti-émeute, un taser et un pistolet PBST. Ces armes se trouvent dans la salle de sécurité des installations de Pinewood. Les cadets ne sont pas autorisés dans les salles d'arsenal des Tiers.

## Quand les points seront-ils enregistrés ?
Cela dépend si l'entraîneur est disponible et a le temps d’enregistrer des points. Et nous ne le dirons jamais assez, mais **ne demandez pas aux entraîneurs d'enregistrer les points**. Cela sera ennuyeux pour les entraîneurs qui enregistrent les points en question. Et c’est généralement le spam si beaucoup de gens disent **SIR POINTS PLEASE LOG POINTS**. Les entraîneurs peuvent enregistrer des points, ce qui signifie qu’ils peuvent également les **ENLEVER**. Comme un avertissement que je peux vous donner, **NE PAS** demander que les points soient enregistrés, à moins qu’un entraîneurs ne vous demande de leur demander de le faire (Prenez l’image comme exemple)
<img class="no-medium-zoom zooming" src="https://i.imgur.com/yNHkmzm.png" alt="demo" width="1228" />

## Les Entraînements individuels (ST)
Lorsque vous vous entraînez par vous mêmes, vous pouvez gagner des points sans être entraîné par un entraîneur, les points peuvent changer en fonction de votre rang. Si vous progressez dans les niveaux (Tiers), vous gagnerez également moins de points aux entraînements individuels. Cela est dû au fait que vous devriez être en mesure de faire le niveau supérieur si vous êtes un haut Tier.

Une image créée par **vgoodedward** montre la bonne façon de procéder : <img class="no-medium-zoom zooming" src="https://i.imgur.com/ho3u0a5.png" alt="demo" width="1228" />

