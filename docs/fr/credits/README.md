---
sidebar: auto
---

# Crédits là où ils sont dus
Nous aimerions remerciez ces personnes pour nous aider avec le manuel.

## Administration + Développement
* Stefano / superstefano4

## Traductions
:::tip Traductions sont en cours de réalisation En raison du fait que les traductions doivent être approuvées manuellement. Cette section a été supprimée **TEMPORAIREMENT**. Il sera de retour dès que la première langue aura atteint 100% de complétion. Si vous souhaitez aider, veuillez vous rendre sur [ce site Web](https://translate.pinewood-builders.ga). Et soumettez une langue à utiliser. (Contactez-moi sur [cet e-mail](mailto:stefano@stefanocoding.me), pour ajouter votre langue) :::


## Relecteurs
### HoS
* Csd | Csdi

### PIA
* Omni | The GreatOmni
### Entraineurs
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
