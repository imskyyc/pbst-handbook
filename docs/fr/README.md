---
home: vrai
heroImage: /PBST-Logo.png
actionText: Allons-y 
actionLink: pbst/
---

::: ATTENTION : A partir de
 - `Format de date européenne: 9-3-2020`
 - `Format de la date américaine: 3-9-2020`

Cette version du manuel est maintenant un manuel officiel du PBST. Comme indiqué par Csdi à cette date spécifique: ![img](https://i.imgur.com/ZVyPdZb.png)

REMARQUE :Le manuel officiel peut être trouvé [ici](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368). Merci de soutenir également la version officielle :smile: :::

::: AVERTISSEMENT ! Veuillez lire attentivement toutes les règles. Toute modification des règles sera clarifiée. Les règles ont été catégorisées en fonction du groupe auxquelles elles s'appliquent. Toutefois, certaines règles peuvent s'appliquer à d'autres groupes que la PBST, auquel cas elles seront précisées. Les sanctions pour avoir enfreint une règle dépendent de la règle et de la sévérité. Les sanctions peuvent aller d'un simple avertissement, d'une rétrogradation ou même d'un ajout sur la liste noire de la PBST. :::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>