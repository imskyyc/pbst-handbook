
# Podręcznik PBST
## Jednolity
Oficjalne mundury PBST można znaleźć w sklepie [tutaj](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Jeśli nie masz Robux, większość zaplecza jest wyposażona w jednolitych girowników, którzy mogą cię podążać. Nadal zaleca się kupowanie jednolitości dla Twojej wygody.

Podczas patrolowania obiektu z drewna sosnowego należy nosić oficjalny wzór PBST.

**Lokalizacje jednolitych givers w obiektach**:
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) From the main spawn through the lobby, up the elevator (follow ‘Security Sector + Cafe)’, in the PBST room to the right
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility) From the main spawn into the room marked ‘PB SEC’, to the hallway between the baton giver and the coffee machine, changing room on the left
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ) 4th floor, from elevators to the glass doors on the left, blue doors on the left
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility) At the right side of the spawn building
* [PBST Activity Center:](https://www.roblox.com/games/1564828419/PBST-Activity-Center) Into the main hallway of the lobby

Nie zezwala się na noszenie jednolitości PET, jedyny wyjątek dotyczy PBCC, gdy temperatura rdzenia uniemożliwia noszenie standardowego rdzeniowego kostiumu, a kombinezon Hazmat PET jest jedynym sposobem dotarcia do rdzenia (3000C+). Gdy podstawowa temperatura pozwala ponownie na standardowy rdzeń (3000C-), musisz wrócić na ten garnitur.

## W służbie
Musisz nosić oficjalne ujednolicenie PBST, aby można było uznać je za obowiązki. Twój znacznik rangi musi być włączony i ustawiony na bezpieczeństwo (co jest domyślne), chociaż posiadanie tylko znacznika rangi nie sprawia, że jesteś obowiązkiem.

Jeśli jesteś ustawiony na **TMS** lub **PET**, wykonaj polecenie `!setgroup PBST`

Jako urzędnik PBST ma pan chronić zakład przed wszelkimi zagrożeniami, które się pojawiają. Kiedy musisz ewakuować, najtrudniej pomóc innym.

Aby zrezygnować z obowiązków, musisz odwiedzić pokój bezpieczeństwa i usunąć swoją broń jednorodną i PBST. Zresetuj znak, jeśli jest wymagany. Zalecane jest również usunięcie tagu rangi bezpieczeństwa, używając komendy „`! etgroup [PBST/TMS/PET/Group]`" aby zmienić na neutralną grupę, taką jak PET, PB lub jakakolwiek inna grupa Pinewood, w której się znajdujesz, lub użyj „`! anktag wył`", aby całkowicie wyłączyć znacznik rangi.

## Rdzeń Sterownika Pinewood (PBCC)
[Pinewood Computer Core](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) jest najważniejszym miejscem do patrolowania. Twoim głównym celem jest zapobieżenie wytapianiu lub zamrażaniu rdzenia. Poniższy wykres pokazuje, jak różne ustawienia wpływają na temperaturę. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228" />

PBCC charakteryzuje się również różnymi „klęskami”, z których część wymaga od PBST ewakuacji odwiedzających. W przypadku gwałtownego wzrostu plazmy, trzęsienia ziemi lub promieniowania, stwarzają zagrożenie dla bezpieczeństwa.

**Extra info**: To calculate the TPS (temperture per second) I've added a calculator: <Calculator />

## Użycie broni
Every PBST member has access to the standard baton, and Tiers receive additional weapons from the blue loadouts. Be sure to have your uniform on when taking these weapons, for you are considered off-duty without uniform and you may not use PBST Weapons off-duty. **PBST weapons may never be used to cause a melt-or freezedown.**

You have to give **two** warnings to people before you can kill them. If a visitor has been killed after three warnings and still returns, you can kill that person *without warning*. On-duty PBST members also have to follow these rules when using non-PBST weapons like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned

In case of emergency, a Tier 4 or higher may grant you permission to restrict a room to **PBST only**. In that case, you can kill anyone who tries to enter and only let *on-duty PBST* in.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. You can, for example, restrict rooms without needing permission, though excessive room restriction may count as "mass random killing".

Mass random killing and spawn killing are always forbidden, use the `!call` command to call PIA to your assistance if someone is mass random killing or spawn killing.

## Syndykat Mayhem
The Mayhem Syndicate (**TMS**) is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. You can recognize them by the red rank tag. They often join a game in a raid, and when this happens you are advised to call for backup.

The only place where TMS may not be killed (aside from spawn) is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts.

If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-raid.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to the side for either TMS or PBST in a raid might get you set on KoS (Kill on Sight) by the other side.

## Zabij na Celowcu (KoS)
Any member of TMS is to be killed on sight.

Mutants are KoS as well. **PBST members may not become mutants while on duty.**

On-duty PBST may not put anyone else on KoS unless permission has been granted by a Tier 4+.

## Training
::: warning NOTE: Instructions in training are given in English, so understanding English is required. :::

Tier 4+ can host a training where you can earn [points]((../ranks-and-ranking-up/)). The schedule of these can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Training Rules**:
* Słuchaj hosta i realizuj jego zamówienia.
* Nie pobieraj twoich uzbrojenia, host poda ci to, czego potrzebujesz w każdej aktywności.
* W szkoleniu należy zawsze nosić ustabilizowane PBST.
* Trainery mogą aktywować uprawnienie do mówienia (PTS), co oznacza, że czat dostanie cię karę. Czat ‘`;pts request`’ aby uzyskać zgodę, jeśli to jest włączone.
* Niektóre z nich oznaczone są jako szkolenie dyscyplinarne, które jest bardziej rygorystyczne i bardziej skoncentrowane na dyscyplinze. Jeśli zachowujesz się źle, zostaniesz wyrzucony z szkolenia.
* Najpóźniej raz w tygodniu odbędzie się Szkolenie Hardcore. W tym szkoleniu nawet najmniejszy błąd sprawi, że się wyrzucisz. Jeśli przeżyjesz do końca, możesz zarobić do 20 punktów.

More information about points can be found [here](../ranks-and-ranking-up/)

## Poziomy
You will receive the next Tier rank once you reach the required amount of points. With your new rank comes a new loadout in Pinewood facilities: Tier 1 gives you a more powerful baton, a riot shield, a taser, and a PBST pistol. With Tier 2 you receive all the former plus a rifle. At Tier 3 you receive all the former plus a submachine gun. These weapons can be found in the Security room at Pinewood facilities, and Cadets are not allowed in there.

::: tip Recent change! As of `02/28/2020`. Tier **1** evaluations are now a thing. It you want to go from `Cadet` -> `Tier 1`. You need to complete an evaluation.

You can read more about it [here](../ranks-and-ranking-up/#tier-evaluations). :::

Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the `!call PBST` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at training, where you will be given temporary admin powers which are strictly for that specific training. Abuse of these powers will result in severe punishment.

## Poziom 4 (alias Obrona specjalna, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

Like Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host training with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trenery
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Przypomnienia dla wszystkich członków PBST
  * Proszę zawsze przestrzegać [zasad społeczności ROBLOX](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules).
  * Bądź szanowany dla innych graczy, nie zachowuj się tak, jakbyś był szefem gry.
  * Postępuj zgodnie z rozkazami każdego, kto cię zrzeka.

::: warning NOTE: These rules can be changed at any time. So please check every now and then for a update on the handbook. :::
