# Body a řazení ranku
## Zkratky
 * ST (Self-Training)
 * PBST (Pinewood Builders Security Team)

## Jak si vydělat body
Body můžete vydělat pár cestami, můžete udělat [ST](#abbreviations). Buďte na tréninkách, nebo buďte v událostech jako napadení, a ostatní věci. Podle toho co děláte tak podle toho také dostanete body. Být v napadení (Na [PBST](#abbreviations) straně).

Tréninky jsou také možnost, tréninky jsou vyhlášeny na skupinové zdi. Tréninky mohou být organizovány:
 * Trenéry (Trainer)
 * Tier 4

Kdokoli může být asistent v tréninku, ale pořadač/Trenér si vybere kdo bude asistent, ptaní trenéra vám nepomůže. Toto vám jenom zmenší šance v tom že budete asistent, více informací můžete najít [tady](../handbook/#training)

Jako poslední možnost, můžete zkusit [ST](#abbreviations). Tyto tréninky jsou udělané přesně pro lidi aby mohli si vydělat body jinou cestou,a aby jste si vyzkoušely specifickou aktivitu. Více informací bude vydáno [tady](#self-trainings)

## Žvětšení ranku

Ranking up requires points, which can be earned in training, [ST](#abbreviations)'s and patrols. If you get the required amount of points for the next rank, you’ll be promoted. The required amount of points for any rank can be found at the spawn at [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center), ~~though you can ignore the part of evaluations, those are only in place to become Tier 4~~. This has now changed. Please read the message below...

:::danger Keep in mind! When you're a `Tier 1+` you may be punished harder for mistakes you make, you're supposed to be a role model for all the cadets and visitors in **Pinewood Facilities**. This has no exeption (Unless stated otherwise as the T4 uniform rule) :::

::: warning WARNING Please look out with current promotions, there has been announcement at the PBST Discord. This told that **Tier 1** will need to pass an evaluation. As told by this message: <img class="no-medium-zoom zooming" src="https://i.imgur.com/MxDifjh.png" alt="demo" width="1228" /> :::

## Tier Evaluations
Once you reach 100 points, you must participate in a Tier evaluation if you want to get the **Tier 1** rank. As with normal trainings, these can be found on [the schedule](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). They will be hosted regularly to accommodate all timezones.

There is a specialized server for Tier evaluations, use the command `!pbstevalserver` to get there. This only works if you have 100+ points.

**A Tier evaluation consists of 2 parts**:
  1. A quiz and a test on patrolling skills.
  2. The training rules will be heavily enforced in this eval, not following them will result in an immediate fail.

During the quiz, you will receive 8 questions about various topics including this handbook and training rules. The questions will vary in difficulty, some are easy but others require more thinking. You need to score at least 5/8 to pass. Answering must be done privately through whisper chat or a Private Message system sent by the host.

During the patrol test, you will receive **Tier 1** loadouts and be tested on skills like your abilities in combat, teamwork, and following the handbook correctly. A number of Tier 3 will work against you in this part, as they will try to bring the core to melt- or freezedown.

Once you have passed the Tier evaluation, you will be promoted to **Tier 1**. With this rank, you will get a new loadout in Pinewood facilities: a more powerful baton, a riot shield, a taser, and a PBST pistol. These weapons can be found in the Security room at Pinewood facilities. Cadets are not allowed in the loadout rooms of Tiers.

## When will points be logged?
This depends on what trainer is availible and has the time to log points. And we can not say this enough, but **do not ask trainers for points to be logged**. This will only be annoying for the trainers logging the points in question. And it's generally spamming if alot of people say **SIR SIR POINTS PLEASE LOG POINTS**. Trainers are able to log points, this means they can also **SUBTRACT** them. As a warning I can give you, **DON'T** ask for points to be logged, unless a trainer tells you to ask them to be logged (Take image as example)
<img class="no-medium-zoom zooming" src="https://i.imgur.com/yNHkmzm.png" alt="demo" width="1228" />

## Self Trainings
When you're self training, you able to earn points without being in a training, the points can change depending on your rank. If you progress in Tiers, you also earn less points in self training. This is due to the fact that you should be able to do a higher level if you're a higher Tier.

An image created by **vgoodedward** shows this is a good way: <img class="no-medium-zoom zooming" src="https://i.imgur.com/ho3u0a5.png" alt="demo" width="1228" />

