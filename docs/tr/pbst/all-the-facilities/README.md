# Bütün tesisler

:::danger Please note: This part of the handbook **may** get some changes to their descriptions. Like locations for uniform givers. :::

## Pinewood Builders Bilgisayar Çekirdeği (PBBÇ)
Patlamak üzere olan bir gizli yer altı tesisini gez! Görevin, bir süperbilgisayarın aşırı ısınmasını önlemek. Tesisin her köşesini gez, felaketlerden sağ çık, her düğmeye bas, sırları keşfet, ve belki sağ çıkabilirsin.

📙 Bu oyun Roblox'un En İyi Macera Oyunları (Roblox Top Adventure Games) kitabında rol almıştır, bir göz at!

- Ekstra özellikler için aşağıdaki ikona tıklayarak Pinewood Builders'a katılın.

- Yardımcılar + Jenerik: Özellikle Irreflexive, Lenemar ve Csdi Eşyalar ve silahlar için: SigmaTech, Terrariabat, TheGreatOmni, Unsayableorc. Müziğini kullanmamıza izin verdiği için Jay Cosmic'e teşekkür ederiz.

## Pinewood Builders Advantage Uzay Mekiği (PBAUM)
Uzaya Advantage Uzay Mekiği'ni fırlatıp sürün! Daha sonra yörüngeye uydu yerleştirme görevini tamamla ve sonra ekibinle birlikte dünyaya geri gel; Uzay İstasyonuna, Ay ve diğer gezegenlere ulaş; ve daha sonra dünya dışı silahlar keşfetmek için uzaylı ana gezegenine cesurca roket sür!

Advantage Projesi, birbirine bağlı lazer uydu ağları fırlatmadaki eforumuz. Bu oyun Roblox Spotlight blogu ve 2013 London Bloxcon'da rol almıştır. 28/05/2012 tarihinde topluma sunulmuştur.

- Ekstra özellikler için aşağıdaki ikona tıklayarak Pinewood Builders'a katılın.

- Yardımcılar + Jenerik: Silahlarda yardım için RedDwarfIV, Irreflexive, Lenemar, Csdi.

## Pinewood Builders Araştırma Tesisi (PBAT)
Welcome to Pinewood Research Facility, located in a secret part of the Sahara Desert and above the underground Pinewood Computer Core it serves as the hub between our main facilities and games making up the Sahara complex - State of the art mega-structures and shuttle launch sites, as well as the home of our supercomputer mainframe and satellite communications for our space projects.

You can come and visit the exhibition, where we show you our past technology and creations. This place also features the Pinewood astronaut training centrifuge capable of incredibly high G's, so buckle up.

This facility was first proposed in 2010, after decommissioning the original 2008 Pinewood Labs, the 2009 Research Facility, and the facilities at the 2009 HQ. Any areas still under construction will be blocked off with a white wall.

## Pinewood Headquarters (PBHQ)
Welcome to our group headquarters. First built in 2011, this has been the main meeting place and headquarters for Pinewood Builders, located in Tokyo, Japan; we welcome all visitors.

This game was featured in the Roblox Blog 2013, in "[Six ROBLOX Places That'll Make You Say "Whoa"](https://blog.roblox.com/2013/06/six-roblox-places-thatll-make-you-say-whoa/)".

## Pinewood Builders Data Storage Facility (PBDSF)
Located in Wyoming, this dedicated data center stores points and other information for divisions of Pinewood Builders.

Irreflexive - Database Scripter Csdi - Main Builder / UI Design spyagent388 - Assistant Builder SADENNING - Minor Building, Ideas

## PBST Training Facility (PBSTTF)
Project CRATER 2015, otherwise known as the official PBST Training Facility is located just outside of Pinewood Research Facility in the great Sahara. The impact crater plays host to a wide variety of training activities and scenarios, as well as gladiator arenas to separate the skilled.

The climate is maintained thanks to a huge air conditioned bio-dome above the crater and access is via a state of the art monorail system. The PBST Shooting dome is also located here.

## PBST Activity Center (PBSTAC)
Brand new training, self-training, and patrol zone! This facility contains five times the activities used in the former "Project CRATER", and many activities in the simulation room. Join PBST to gain access to the facility, and to attend awesome trainings!

Officially released to the public as of June 12th 2018.

## PBST Hub (PBSTH)
This hub serves as a teleportation place to all PBST games, patrol facilities, and other games! It also serves as a central information place for new members to learn about PBST!
