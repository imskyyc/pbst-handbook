
# Buku pegangan PBST
## Seragam
Seragam PBST resmi dapat ditemukan di toko [di sini](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Kalau Anda tidak punya Robux, sebagian fasilitas dilengkapi dengan pemberi seragam untuk melanjutkan pekerjaan. Masih disarankan agar Anda membeli seragam untuk kenyamanan Anda.

Anda harus memakai seragam PBST resmi saat berpatroli di fasilitas Pinewood.

**Lokasi pemberi seragam dalam fasilitas**:
* [Pinewood Computer Core/Inti Komputer Pinewood](https://www.roblox.com/games/17541193/Pinewood-Computer-Core): Dari spawn utama melalui lobi, ke atas elevator/lift (ikuti ‘Security Sector + Cafe’), di dalam kamar PBST ke kanan
* [Pinewood Research Facility/Fasilitas Penelitian Pinewood](https://www.roblox.com/games/7692456/Pinewood-Research-Facility): Dari spawn utama ke dalam kamar yang ditandai ‘PB SEC’, melalui lorong di antara pemberi baton dan mesin kopi, kamar ganti di kiri
* [Pinewood Builders HQ/Kantor Pusat Pinewood](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ): Lantai 4, dari elevator/lift ke pintu kaca di kiri, pintu biru di kiri
* [PBST Training Facility/Fasilitas Pelatihan PBST](https://www.roblox.com/games/298521066/PBST-Training-Facility): Di sisi kanan gedung spawn
* [PBST Activity Center/Pusat Pelatihan PBST](https://www.roblox.com/games/1564828419/PBST-Activity-Center): Melalui lorong utama lobi

Memakai seragam PET tidak diperbolehkan, satu-satunya pengecualian adalah di PBCC saat suhunya inti membikinnya mustahil untuk memakai jas inti standar dan jas Hazmat PET adalah satu-satunya cara untuk masuk ke inti (3000C+). Begitu suhu inti memungkinan untuk jas inti standar lagi (3000-), Anda harus mengganti ke setelan tersebut.

## Bertugas
Anda harus memakai seragam PBST resmi untuk dipertimbangkan bertugas. Rank tag Anda juga harus dinyalakan dan diatur ke Security (yang default), tapi kalau hanya mempunyai rank tagnya tidak membikin Anda bertugas.

Kalau Anda diatur ke **TMS** atau **PET**, mohon jalankan command `!setgroup PBST`

Sebagai Petugas PBST yang bertugas, Anda harus melindungi fasilitasnya dari ancaman yang muncul. Kalau anda harus mengungsi, berusaha sekuat tenaga untuk membantu yang lain.

Untuk pergi bertugas, Anda harus mengunjungi kamar Security dan lepaskan seragam dan senjata PBST. Reset character jika diperlukan. Sangat disarankan Anda menghapus rank tag Security Anda, dengan menggunakan command “`!setgroup [PBST/TMS/PET/Group Name]`” untuk mengganti ke grup netral seperti PET, PB atau grup Pinewood lain yang Anda berada, atau pakai command “`!ranktag off`” untuk mematikan rank tag sepenuhnya.

## Pinewood Computer Core/Inti Komputer Pinewood (PBCC)
[Pinewood Computer Core/Inti Komputer Pinewood](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) adalah tempat yang sangat penting untuk berpatroli. Tujuan Anda di sini adalah untuk mencegah intinya dari meleleh atau membeku. Chart tersebut menunjukkan kepada Anda bagaimana pengaturan yang beda mempengaruhi suhu. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228" />

PBCC juga menampilkan berbagai ‘bencana’, beberapa yang membutuhkan PBST untuk mengungsi pengunjung. Dalam hal terjadi lonjakan plasma, gempa bumi atau banjir radiasi, bawa orang yang dalam bahaya ke keamanan.

**Informasi Tambahan**: Untuk menghitung TPS (temperature per second/suhu per detik) saya telah menambahkan kalkulator: <Calculator />

## Penggunaan senjata
Semua anggota PBST memiliki akses untuk baton standar, dan Tiers/Tingkatan menerima senjata tambahan dari loadouts/pemuatan biru. Pastikan Anda memakai seragam saat mengambil senjata, Anda dianggap tidak bertugas tanpa seragam dan Anda tidak boleh menggunakan senjata PBST saat tidak bertugas. **Senjata PBST tidak boleh digunakan untuk menyebabkan Meltdown atau Freezedown.**

Anda harus memberikan **dua** peringatan ke orang sebelum Anda boleh membunuh mereka. Jika pengunjung sudah dibunuh setelah tiga peringatan dan masih kembali, Anda boleh membunuh orang tersebut *tanpa peringatan*. Anggota PBST yang bertugas juga harus mengikuti aturan ini saat menggunakan senjata yang bukan dari PBST seperti gamepass OP Weapons, atau pistol yang diperoleh dari Credits PBCC atau secara acak

Dalam keadaan darurat, Tier 4 atau tingkat lebih tinggi dapat memberikan Anda izin untuk membatasi kamar ke **PBST only/PBST saja**. Dalam hal itu, Anda boleh membunuh siapa saja yang mencoba untuk masuk dan hanya bolehkan *on-duty PBST/PBST bekerja* masuk.

Saat tidak bekerja, Anda dapat menggunakan senjata yang bukan dari PBST, meskipun Anda mempunyai kebebasan dengan penggunaan Anda. Anda boleh, sebagai contoh, membatasi kamar tanpa membutuhkan izin, meskipun batas kamar yang berlebihan dapat dihitung sebagai “pembunuhan acak massal”.

Pembunuhan acak massal dan pembunuhan spawn selalu dilarang, gunakan command `!call` untuk memanggil PIA atas kebantuan Anda jika ada yang pembunuhan acak massal atau pembunuhan spawn.

## The Mayhem Syndicate/Sindikat Kekacauan
The Mayhem Syndicate/Sindikat Kekacauan (**TMS**) adalah seberangnya PBST. Saat kami berniat untuk melindungi intinya, mereka berniat untuk melelehkan atau membekukannya. Anda bisa kenali mereka dari rank tag merah. Mereka sering ikuti game dalam serangan, dan saat ini terjadi Anda disarankan untuk memanggil cadangan.

The only place where TMS may not be killed (aside from spawn) is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts.

If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-raid.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to the side for either TMS or PBST in a raid might get you set on KoS (Kill on Sight) by the other side.

## Kill on Sight (KoS)
Any member of TMS is to be killed on sight.

Mutants are KoS as well. **PBST members may not become mutants while on duty.**

On-duty PBST may not put anyone else on KoS unless permission has been granted by a Tier 4+.

## Training
::: warning NOTE: Instructions in training are given in English, so understanding English is required. :::

Tier 4+ can host a training where you can earn [points]((../ranks-and-ranking-up/)). The schedule of these can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Training Rules**:
* Listen to the host and follow his/her orders.
* Do not take your loadouts, the host will give you what you need in every activity.
* Always wear a PBST uniform in the training.
* Trainers may activate Permission to Speak (PTS), which means chatting will get you punished. Chat ‘`;pts request`’ to get permission if this is on.
* Some are marked as Disciplinary Training, which is stricter and more focused on discipline. You’ll be kicked from the training if you misbehave.
* Once a week at most, a Hardcore Training will be hosted. In this training, even the slightest mistake will get you kicked. If you ‘survive’ to the end, you can earn up to 20 points.

More information about points can be found [here](../ranks-and-ranking-up/)

## Tiers
You will receive the next Tier rank once you reach the required amount of points. With your new rank comes a new loadout in Pinewood facilities: Tier 1 gives you a more powerful baton, a riot shield, a taser, and a PBST pistol. With Tier 2 you receive all the former plus a rifle. At Tier 3 you receive all the former plus a submachine gun. These weapons can be found in the Security room at Pinewood facilities, and Cadets are not allowed in there.

::: tip Recent change! As of `02/28/2020`. Tier **1** evaluations are now a thing. It you want to go from `Cadet` -> `Tier 1`. You need to complete an evaluation.

You can read more about it [here](../ranks-and-ranking-up/#tier-evaluations). :::

Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the `!call PBST` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at training, where you will be given temporary admin powers which are strictly for that specific training. Abuse of these powers will result in severe punishment.

## Tier 4 (aka Special Defense, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

Like Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host training with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Reminders to all PBST members
  * Please abide by the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) at all times.
  * Be respectful to your fellow players, don’t act like you’re the boss of the game.
  * Follow the orders of anyone who outranks you.

::: warning NOTE: These rules can be changed at any time. So please check every now and then for a update on the handbook. :::
