# Todos las facilidades

:::danger Ten en cuenta: Esta parte del manual **puede** obtener algunos cambios en sus descripciones. Como lugares para los gigantes uniformes. :::

## Base de computadora Pinewood (PBCC)
¡Explora una instalación subterránea secreta que está a punto de explotar! Su misión es evitar que un superordenador se sobrecalente. Explora todos los rincones de la instalación, sobrevive a los desastres, pulsa cada botón, descubre secretos y ¡simplemente hazlo vivo!

📙 Este juego fue destacado en el libro Top Adventure Games de Roblox, ¡entonces echa un vistazo!

- ¡Haz clic en el icono de abajo para unirte a los Constructores de Pinewood para obtener beneficios adicionales!

- Ayudantes + Créditos: Irreflexive, Lenemar y Csdi especialmente. SigmaTech, Terrariabat, TheGreatOmni, Unsayableorc para ayuda con armas y objetos. Gracias Jay Cósmico por el permiso para usar tu canción.

## Ventaja del transbordador espacial Pinewood (PBSSA)
¡Lanza y monta la Azotaje Espacial Ventaja en el espacio! Luego completa tu misión de despliegue de satélite en órbita y vuelve a la Tierra con tu equipo, accede a la Estación Espacial, la Luna y otros planetas, y luego rocear valientemente hasta el mundo extranjero de origen para descubrir armas extra-terrestres!

¡Proyecto Ventaja, nuestro esfuerzo por lanzar una red de satélites láser interconectados! Este juego se destacó en el blog Roblox Spotlight y el Bloxcon de Londres de 2013. Publicado al 28/05/2012

- ¡Haz clic en el icono de abajo para unirte a los Constructores de Pinewood para obtener beneficios adicionales!

- Ayudantes + Créditos: RedDwarfIV, Irreflexive, Lenemar, Csdi para ayuda con armas

## Pinewood Research Facility (PBRF)
Bienvenidos a Facilidad de Investigación de Pinewood, ubicado en una parte secreta del desierto de Sahara y sobre el Base de computadora Pinewood subterráneo. Sirve como el centro entre nuestros facilidades principales y juegos que componen el complejo Sahara - Megaestructuras de última generación y sitios de lanzamiento del transbordador,  además de la casa de nuestro superordenador mainframe y comunicaciones satelitales para nuestros proyectos espaciales.

Puede venir y visitar la exposición, donde le mostramos nuestra tecnología y creaciones pasadas. Este lugar también publicas el centrifuge de entrenamiento de astronautos de Pinewood, capz de Gs increíblemente alto, entonces ponerse tu cinturón de seguridad.

Esta instalación se propuso por primera vez en 2010, después de desmantelar los Pinewood Labs originales, la Facilidad de Investigación 2009 y las facilidades en la sede de 2009. Cualquier área todavía en construcción será bloqueada con una pared blanca.

## Pinewood Headquarters (PBHQ)
Bienvenidos a nuestra sede de grupo. Construido en 2011, este ha sido el principal punto de encuentro y sede para Pinewood Builders, ubicado en Tokyo, Japón; damos la bienvenida a todos los visitantes.

Este juego se destacó en el Blog de Roblox 2013, en "[Seis Lugares ROBLOX que te harán decir "Whoa"](https://blog.roblox.com/2013/06/six-roblox-places-thatll-make-you-say-whoa/)".

## PBST Base de datos de los Puntos (PBDSF)
Ubicado en Wyoming, este centro de datos dedicado almacena puntos y otra información para divisiones de Pinewood Builders.

Irreflexive - Programador del la base de datos Csdi - Constructor principal spyagent388 - Constructor asistente SADENNING - Edificio menor, ideas

## PBST Training Facility (PBSTTF)
Proyecto CRATER 2015, también conocido como la facilidad de entrenamiento oficial es situado a las afueras de Pinewood Research Facility en el Sahara estupendo. El cráter de impacto es el anfitrión de una amplia variedad de actividades de entrenamiento y escenarios, así como de áreas de gladiadores para separar a los expertos.

El clima se mantiene gracias a un enorme bio-dome con aire acondicionado por encima del cráter y acceso es a través de un sistema monorraíl de última generación. El domo di tiro de PBST también se encuentra aquí.

## PBST Activity Center (PBSTAC)
¡Nuevo entrenamiento, autoformación, y zona de patrocinio! Esta facilidad contiene cinco veces la actividades utilizadas en el anterior "Project CRATER", y muchas actividades en la sala de simulación. ¡Únete a PBST para acceder a la facilidad y asistir a entrenamientos impresionantes!

Oficialmente liberado al público a partir del 12 de junio de 2018.

## PBST Hub (PBSTH)
Este hub sirve como un lugar de teletransporte a todos los juegos PBST, facilidades de patrullas, y otros juegos. ¡También sirve como un lugar central de información para que miembros nuevos aprendan sobre PBST!
