
# Manual de PBST
## Uniforme
Los uniformes oficiales de PBST se pueden encontrar en la tienda [aquí](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Si usted no tiene Robux, la mayoría de las facilidades están equipadas con dosificadores de uniformes para llevarle a cabo. Todavía se recomienda que compre un uniforme para su comodidad.

Debes llevar un uniforme oficial de PBST mientras patrullaba una facilidad de Pinewood.

**Ubicaciónes de los dosificadores de uniformes en facilidades**:
* [Base de computadora Pinewood:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) Desde el spawn principal a través del lobby, subir el ascensor (siga "Security Sector + Cafe"), en la sala PBST a la derecha.
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility) Desde el spawn principal hasta la sala marcó 'PB SEC', al pasillo entre el portero y la máquina de café, vestuario a la izquierda.
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ) Cuarto piso, desde ascensores a las puertas de cristal, puertas azules a la izquierda.
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility) En el lado derecho del edificio de spawn.
* [PBST Activity Center:](https://www.roblox.com/games/1564828419/PBST-Activity-Center) En el pasillo centro del lobby

No se permite usar un uniforme PET, la única excepción es en PBCC cuando la temperatura del núcleo hace imposible usar un traje de núcleo estándar y un traje PET Hazmat es la única manera de entrar en el núcleo (3000C+). Una vez que el núcleo temporal permite un traje de núcleo estándar de nuevo (3000C-), usted debe volver a dicho traje.

## Trabajando como seguridad
Necesitas llevar un uniforme oficial de PBST para ser considerado en funcionamiento. Tu etiqueta rank también debe estar en Seguridad (que es el valor predeterminado), aunque tener sólo la etiqueta rank no te hace de trabajo.

Si estás configurado en **TMS** o **PET**, por favor ejecuta el comando `!setgroup PBST`

Como Oficial de PBST en funciones, usted debe proteger la facilidad de cualquier amenaza que surja. Cuando tenga que evacuar, pruebe lo más duro posible para ayudar a los demás.

Para abandonar tu cargo, tiene que visitar la sala de Seguridad y quitar sus armas  y uniformes de PBST. Restablecer carácter si es necesario. Es altamente recomendable que también elimine su etiqueta Security usando el comando “`!setgroup [PBST/TMS/PET/Nombre de equipo]`" para cambiar a un grupo neutral como PET, PB o cualquier otro grupo de Pinewood en el que se encuentre, o usar "`!ranktag off`” para desactivar la etiqueta rank completamente.

## Base de computadora Pinewood (PBCC)
El [Base de computadora Pinewood](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) es el lugar más importante para patrullar. Su principal objetivo aquí es evitar que el núcleo se derrite o se congele. El siguiente gráfico muestra cómo diferentes ajustes afectan la temperatura. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228" />

El PBCC también incluye varios “desastres”, algunos de los cuales requieren el PBST para evacuar a los visitantes. En caso de que se produzca un aumento del plasma, un terremoto o una inundación de radiación, llevar a la gente en peligro a la seguridad.

**Información adicional**: Para calcular el TPS (temperatura por segundo) He añadido una calculadora: <Calculator />

## Uso de armas
Todos los miembros de PBST tienen acceso a la porción estándar, y los Tiers reciben armas adicionales de los dosificadores azules. Asegúrate de tener tu uniforme al tomar estas armas, para que usted sea considerado fuera de servicio sin uniforme y usted no puede usar armas PBST fuera de servicio. **Armas de PBST nunca pueden ser usadas para causar un derretimiento o congelación.**

Tienes que dar a **dos** advertencias a la gente antes de que puedas matarlas. Si un visitante ha sido asesinado tras tres advertencias y todavía regresa, puedes matar a esa persona *sin avisar*. On-duty PBST members also have to follow these rules when using non-PBST weapons like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned

In case of emergency, a Tier 4 or higher may grant you permission to restrict a room to **PBST only**. In that case, you can kill anyone who tries to enter and only let *on-duty PBST* in.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. You can, for example, restrict rooms without needing permission, though excessive room restriction may count as "mass random killing".

Mass random killing and spawn killing are always forbidden, use the `!call` command to call PIA to your assistance if someone is mass random killing or spawn killing.

## The Mayhem Syndicate
The Mayhem Syndicate (**TMS**) is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. You can recognize them by the red rank tag. They often join a game in a raid, and when this happens you are advised to call for backup.

The only place where TMS may not be killed (aside from spawn) is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts.

If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-raid.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to the side for either TMS or PBST in a raid might get you set on KoS (Kill on Sight) by the other side.

## Kill on Sight (KoS)
Any member of TMS is to be killed on sight.

Mutants are KoS as well. **PBST members may not become mutants while on duty.**

On-duty PBST may not put anyone else on KoS unless permission has been granted by a Tier 4+.

## Training
::: warning NOTE: Instructions in training are given in English, so understanding English is required. :::

Tier 4+ can host a training where you can earn [points]((../ranks-and-ranking-up/)). The schedule of these can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Training Rules**:
* Listen to the host and follow his/her orders.
* Do not take your loadouts, the host will give you what you need in every activity.
* Always wear a PBST uniform in the training.
* Trainers may activate Permission to Speak (PTS), which means chatting will get you punished. Chat ‘`;pts request`’ to get permission if this is on.
* Some are marked as Disciplinary Training, which is stricter and more focused on discipline. You’ll be kicked from the training if you misbehave.
* Once a week at most, a Hardcore Training will be hosted. In this training, even the slightest mistake will get you kicked. If you ‘survive’ to the end, you can earn up to 20 points.

More information about points can be found [here](../ranks-and-ranking-up/)

## Tiers
You will receive the next Tier rank once you reach the required amount of points. With your new rank comes a new loadout in Pinewood facilities: Tier 1 gives you a more powerful baton, a riot shield, a taser, and a PBST pistol. With Tier 2 you receive all the former plus a rifle. At Tier 3 you receive all the former plus a submachine gun. These weapons can be found in the Security room at Pinewood facilities, and Cadets are not allowed in there.

::: tip Recent change! As of `02/28/2020`. Tier **1** evaluations are now a thing. It you want to go from `Cadet` -> `Tier 1`. You need to complete an evaluation.

You can read more about it [here](../ranks-and-ranking-up/#tier-evaluations). :::

Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the `!call PBST` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at training, where you will be given temporary admin powers which are strictly for that specific training. Abuse of these powers will result in severe punishment.

## Tier 4 (aka Special Defense, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

Like Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host training with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Reminders to all PBST members
  * Please abide by the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) at all times.
  * Be respectful to your fellow players, don’t act like you’re the boss of the game.
  * Follow the orders of anyone who outranks you.

::: warning NOTE: These rules can be changed at any time. So please check every now and then for a update on the handbook. :::
