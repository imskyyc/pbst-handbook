---
sidebar: automático
---

# Créditos a donde deben
Queremos agradecer estas personas para ayudándonos con el manual.

## Backend y desarrollo
* Stefano / superstefano4

## Traducciónes
:::tip Traducciones se están haciendo actualmente debido a que las traducciones necesitan ser aprobadas manualmente. Esta sección ha sido **TEMPORALMENTE** eliminado. Volverá cuando el idioma primero llega 100% finalización. Si quieres ayudar, por favor ve a [este](https://translate.pinewood-builders.ga) sitio web. Y entrega un idioma a utilizar. (Contacta conmigo en [este correo electrónico](mailto:stefano@stefanocoding.me), para añadir tu idioma) :::


## Revisores
### Jefe De Seguridad
* Csd | Csdi

### PIA
* Omni | TheGreatOmni
### Entrenadores
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
