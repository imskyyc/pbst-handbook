module.exports = [{
        text: 'Home',
        link: '/'
    },
    {
        text: 'PBST Handbook',
        link: '/pbst/'
    },
    {
        text: 'Credits',
        link: '/credits/'
    },
    {
        text: 'Pinewood',
        items: [{
                text: 'PET-Handbook',
                link: 'https://pet.pinewood-builders.com'
            },
            {
                text: 'TMS-Handbook',
                link: 'https://tms.pinewood-builders.com'
            }
        ]
    }

]