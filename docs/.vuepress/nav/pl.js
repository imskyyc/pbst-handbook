module.exports = [{
    text: 'Home',
    link: '/'
},
{
    text: 'PBST Handbook',
    link: '/pl/pbst/'
},
{
    text: 'Credits',
    link: '/pl/credits/'
},
{
    text: 'Pinewood',
    items: [{
            text: 'PET-Handbook',
            link: 'https://pet.pinewood-builders.com'
        },
        {
            text: 'TMS-Handbook',
            link: 'https://tms.pinewood-builders.com'
        }
    ]
}
]